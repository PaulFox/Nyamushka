"use strict";

document.addEventListener("DOMContentLoaded", function() {
    var packageCheckboxes = document.querySelectorAll(".package-ui-checkbox");

    for(var i = 0; i < packageCheckboxes.length; i++) {

        var item = packageCheckboxes[i];

        item.addEventListener("change", function() {
            if (this.checked === true) {
                this.nextElementSibling.classList.add("just-selected");
            }
        });

        item.nextElementSibling.addEventListener("mouseleave", function() {
            if (this.classList.contains("just-selected")) {
                this.classList.remove("just-selected");
            }
        });
    }
});